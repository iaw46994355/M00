package servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import beans.Cliente;
import beans.Transaccion;
import dao.TransaccionesDAO;

/**
 * Servlet implementation class downloadJsonTransaction
 */
@WebServlet("/downloadJsonTransaction")
public class downloadJsonTransaction extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public downloadJsonTransaction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		try {	 
			System.out.println("-------------------------");
		Gson gson = new Gson();
		
		String c = ((Cliente)request.getSession().getAttribute("clientSession")).getDni();
		//String transactions = request.getSession().getAttribute("lista").toString();
		List<Transaccion> lTrans = TransaccionesDAO.listaTransacciones(c);
//		System.out.println(lTrans);
		String jsonString = gson.toJson(lTrans);
		System.out.println(jsonString);
        
		String filename = "prueba.xml";
        response.setContentType("application/plain-text");
        response.setHeader("Content-Disposition","attachment;filename="+"prueba");
              
        
        File file = new File(filename);
        FileWriter wr = new FileWriter(file);
        wr.write(jsonString);
        wr.close();       
        FileInputStream fileIn = new FileInputStream(file);
        
        OutputStream out = response.getOutputStream();
        

        byte[] outputByte = new byte[(int)file.length()];

        while(fileIn.read(outputByte, 0, (int)file.length()) != -1)
        {
        	out.write(outputByte, 0, (int)file.length());
        }
     }catch (Exception e) {
    	 System.out.println("-------------------------");
	}
	}
}
