CREATE TABLE cliente (
dni VARCHAR(9) PRIMARY KEY,
nombre VARCHAR(25) NOT NULL,
apellidos VARCHAR(50) NOT NULL,
fecha_nacimiento date NOT NULL,
sexo CHAR NOT NULL CHECK( sexo IN('h','m')),
direccion VARCHAR(100) NOT NULL,
telefono VARCHAR(9) NOT NULL,
contrasena VARCHAR NOT NULL
);

INSERT INTO cliente VALUES ('00000000Z','felipe','6',now(),'h','zarzuela','000000000',md5('1234'));
INSERT INTO cliente VALUES ('00000001Z','Alvaro','Astorero',now(),'h','Alava','100000000',md5('1234'));
INSERT INTO cliente VALUES ('46994355L','Albert','Balbastre Morte','188-12-08','h','Barcelona','601241674',md5('jupiter'));

CREATE TABLE cuenta_corriente(
iban VARCHAR(24) PRIMARY KEY CHECK(length(iban) = 24),
saldo DECIMAL NOT NULL,
cliente VARCHAR(9) REFERENCES cliente(dni)
);

INSERT INTO cuenta_corriente VALUES ('XXXXXXXXXXXXXXXXXXXXXXXX','100','00000000Z');
INSERT INTO cuenta_corriente VALUES ('111111111111111111111111','111','00000001Z');
INSERT INTO cuenta_corriente VALUES ('aaaaaaaaaaaaaaaaaaaaaaaa','110','00000001Z');
INSERT INTO cuenta_corriente VALUES ('aaaaaaaaaaaaaaaaaaaaaaa2','400','46994355L');
INSERT INTO cuenta_corriente VALUES ('bbbbbbbbbbbbbbbbbbbbbbbb','500','46994355L');
INSERT INTO cuenta_corriente VALUES ('cccccccccccccccccccccccc','600','46994355L');

CREATE TABLE movimientos (
id BIGSERIAL PRIMARY KEY,
fecha DATE NOT NULL,
cantidad DECIMAL NOT NULL,
origen VARCHAR(24) CHECK ( length(origen) = 24),
destino VARCHAR(24) CHECK ( length(destino) = 24)
);

INSERT INTO movimientos (fecha,cantidad,origen,destino) 
VALUES (now(), 10, 'XXXXXXXXXXXXXXXXXXXXXXXX' ,'YYYYYYYYYYYYYYYYYYYYYYYY');
INSERT INTO movimientos(FECHA, CANTIDAD, ORIGEN, DESTINO) VALUES(TO_DATE('27/11/2017','DD/MM/YYYY'), 100, 'aaaaaaaaaaaaaaaaaaaaaaa1', 'aaaaaaaaaaaaaaaaaaaaaaa2');
